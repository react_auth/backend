from django.urls import include, path
from rest_framework import routers
from account.views import UserViewSet, me

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)

urlpatterns = [
    path('me/', me),
    path('', include(router.urls)),
]
