from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets, status, permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.test import APIRequestFactory

from .serializers import UserSerializer

factory = APIRequestFactory()
request = factory.get('/')
serializer_context = {
    'request': Request(request),
}


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


@api_view(['POST', ])
@permission_classes([permissions.IsAuthenticated, ])
def me(request):
    user_id = request.user.id
    user = User.objects.filter(pk=user_id)

    if not user.exists():
        return Response({
            "Happy hacks..."
        }, status=status.HTTP_403_FORBIDDEN)

    user = user.first()
    return Response({
        "data": UserSerializer(instance=user, context=serializer_context).data
    }, status=status.HTTP_200_OK)
